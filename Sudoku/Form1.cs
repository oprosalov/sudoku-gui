﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Sudoku
{
    public partial class Form1 : Form
    {
        private const int BOARDLENGTH = 9;
        private int NUMOFHIDDEN = 5;
        private Color COLORHIDDENLABELDEFAULT = Color.SandyBrown;
        private Color COLORSELECTEDLABEL = SystemColors.ActiveCaption;
        private Color COLORCORRECTNUM = Color.LimeGreen;
        private Color COLORINCORRECTNUM = Color.Red;

        private int[,] board = new int[BOARDLENGTH, BOARDLENGTH];
        private Label[,] labels = new Label[BOARDLENGTH, BOARDLENGTH];
        private List<HiddenLabel> labelsHidden = new List<HiddenLabel>();    
        private Random rnd = new Random();
        private bool isLabelSelected = false;
        private bool isLabelClickAllowed = true;
        private Label labelSelected = null;

        private class HiddenLabel
        {
            public int row, col, num;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitData();
            this.Text = "Sudoku by Oleg P.";
            labelInfo.Text = "";
        }

        private void InitData()
        {
            // Initialize board array.
            for (int i = 0; i < BOARDLENGTH; i++)
            {
                for (int j = 0; j < BOARDLENGTH; j++)
                {
                    board[i, j] = 0;
                }
            }

            // Initialize labels array
            for (int i = 1; i < (BOARDLENGTH * BOARDLENGTH) + 1; i++)
            {
                Label lbl = FindControl("label" + i.ToString()) as Label;
                int row = GetLabelRow(lbl);
                int col = GetLabelCol(lbl);
                labels[row, col] = lbl;
                labels[row, col].BackColor = SystemColors.Control;
            }

            // Set all labels.Text to empty
            InitLabels();

            // Initialize labelsHidden array
            for (int i = 0; i < NUMOFHIDDEN; i++) 
            {
                HiddenLabel hl = new HiddenLabel();
                labelsHidden.Add(hl);
            }
            
            // Initialize selected label vars.
            isLabelSelected = false;
            labelSelected = null;
           
            // Random seed
            NewSeed();
        }
        
        private void InitLabels()
        {
            for (int i = 0; i < BOARDLENGTH; i++)
            {
                for (int j = 0; j < BOARDLENGTH; j++)
                {
                    //MessageBox.Show(GetLabelID(labels[i, j]).ToString());
                    labels[i,j].TextAlign = ContentAlignment.MiddleCenter;
                    labels[i, j].Font = new Font(labels[i, j].Font.FontFamily.Name, 14);
                    labels[i, j].Text = "";
                }
            }
        }

        private void SetLabels()
        {
            for (int i = 0; i < BOARDLENGTH; i++)
            {
                for (int j = 0; j < BOARDLENGTH; j++)
                {
                    labels[i, j].Text = board[i, j].ToString();
                }
            }
        }

        private void InitLabelInfoText()
        {
            labelInfo.Font = new Font(labelInfo.Font.FontFamily.Name, 10);
            labelInfo.TextAlign = ContentAlignment.TopLeft;
            labelInfo.Text = "Click empty tile and input number. \n" +
                                   "Completion is checked when all tiles are filled.";
                                    
        }

        private void HideLabels()
        {
            int i = 0;
            do
            {                
                int r = rnd.Next(0, BOARDLENGTH);
                int k = rnd.Next(0, BOARDLENGTH);

                if (labels[r, k].Text != "")
                {
                    // Save hidden label data.
                    labelsHidden[i].num = Convert.ToInt32(labels[r, k].Text);
                    //MessageBox.Show(labels[r, k].Text + "\n" + labelsHidden[i].num.ToString());
                    labelsHidden[i].row = GetLabelRow(labels[r, k]);
                    labelsHidden[i].col = GetLabelCol(labels[r, k]);

                    // Hide and highlight.
                    labels[r, k].Text = "";
                    labels[r, k].BackColor = COLORHIDDENLABELDEFAULT;

                    i++;
                }                
            } while (i < NUMOFHIDDEN);
        }

        private bool IsLabelHidden(Label lbl)
        {
            for (int i = 0; i < NUMOFHIDDEN; i++)
            {
                if (GetLabelRow(lbl) == labelsHidden[i].row &&
                    GetLabelCol(lbl) == labelsHidden[i].col)
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsBoardFull()
        {
            for (int i = 0; i < BOARDLENGTH; i++)
            {
                for (int j = 0; j < BOARDLENGTH; j++)
                {
                    if (labels[i, j].Text == "")
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private int GetLabelID(Label lbl)
        {
            return Convert.ToInt32(Regex.Match(lbl.Name, @"\d+").Value);
        }

        private int GetLabelRow(Label lbl)
        {
            int id = GetLabelID(lbl);
            return (id - 1) / BOARDLENGTH;
        }

        private int GetLabelCol(Label lbl)
        {
            int id = GetLabelID(lbl);
            return (id - 1) % BOARDLENGTH;
        }

        private void NewSeed()
        {
            rnd = new Random();
        }

        private void VerifyVictory()    // Sweet name.
        {
            bool isGameFinished = true;
            for (int i = 0; i < NUMOFHIDDEN; i++)
            {
                int r = labelsHidden[i].row;
                int k = labelsHidden[i].col;

                if (board[r, k] != Convert.ToInt32(labels[r, k].Text))
                {
                    labels[r, k].BackColor = COLORINCORRECTNUM;
                    isGameFinished = false;
                }
            }
            if (isGameFinished)
            {
                for (int i = 0; i < NUMOFHIDDEN; i++)
                {
                    int r = labelsHidden[i].row;
                    int k = labelsHidden[i].col;

                    labels[r, k].BackColor = COLORCORRECTNUM;
                }
                labelInfo.Text = "Hurrah!";
                isLabelClickAllowed = false;
            }
            else
            {
                labelInfo.Text = "Try again!";
            }
        }

        private void buttonGenBoard_Click(object sender, EventArgs e)
        {
            isLabelClickAllowed = true;

            NUMOFHIDDEN = Convert.ToInt16(textBoxNumToHide.Text);
            if (NUMOFHIDDEN > 20)
            {
                MessageBox.Show("Max is 20. Number has been set to default (5).");
                NUMOFHIDDEN = 5;
            }
            textBoxNumToHide.Text = NUMOFHIDDEN.ToString();
            buttonGenBoard.Focus();

            InitData();
            PlaceNum(0, 0);
            SetLabels();
            HideLabels();
            InitLabelInfoText();

            // Debug
            //string s = "";
            //for (int i = 0; i < BOARDLENGTH; i++)
            //{
            //    for (int j = 0; j < BOARDLENGTH; j++)
            //    {
            //        s += Convert.ToString(board[i, j]) + " ";
            //    }
            //    s += "\n";
            //}
            //MessageBox.Show(s);            
        }

        private void label_Click(object sender, EventArgs e)
        {
            Label lbl = (Label)sender;

            // If clicked label is hidden (i.e. can be selected)
            if (IsLabelHidden(lbl) && isLabelClickAllowed)
            {
                // If clicked label was previously selected, deselect it.
                if (isLabelSelected && lbl.BackColor == COLORSELECTEDLABEL)
                {
                    lbl.BackColor = COLORHIDDENLABELDEFAULT;
                    labelSelected = null;
                    isLabelSelected = false;
                    return;     // Ends function 
                }
                // If a different label was previously selected, deselect it.
                if (isLabelSelected)
                {
                    labels[GetLabelRow(labelSelected), GetLabelCol(labelSelected)].BackColor = COLORHIDDENLABELDEFAULT;
                    labelSelected = null;
                    isLabelSelected = false;
                }
                // Select new label.
                lbl.BackColor = COLORSELECTEDLABEL;
                labelSelected = lbl;
                isLabelSelected = true;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            // Enter key triggers buttonGenBoard click
            if (e.KeyCode == Keys.Enter)
            {
                buttonGenBoard.PerformClick();
            }

            bool nonNumberEntered = false;

            //If shift key was pressed, it's not a number. 
            if (Control.ModifierKeys == Keys.Shift)
            {
                nonNumberEntered = true;
            }

            // Determine whether the keystroke is a backspace. 
            if (isLabelClickAllowed && e.KeyCode == Keys.Back)
            {
                try
                {
                    int lblTextLen = labelSelected.Text.Length;
                    labelSelected.Text = labelSelected.Text.Substring(lblTextLen - (lblTextLen - 1));
                }
                catch
                { }

                nonNumberEntered = true;
            }

            if (isLabelClickAllowed && isLabelSelected && !nonNumberEntered && labelSelected != null)
            {
                // Numbers only.
                if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) ||
                    (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9))
                {
                    labelSelected.Text = e.KeyData.ToString().Substring(e.KeyData.ToString().Length - 1);

                    if (IsBoardFull())
                    {
                        VerifyVictory();
                    }
                }

            }
        }

        /********************************************************************************
         Source: http://www.codeproject.com/Articles/44928/FindControl-for-Windows-Forms
        ********************************************************************************/
        Control FindControl(string target)
        {
            return FindControl(this, target);
        }

        static Control FindControl(Control root, string target)
        {
            if (root.Name.Equals(target))
                return root;
            for (var i = 0; i < root.Controls.Count; ++i)
            {
                if (root.Controls[i].Name.Equals(target))
                    return root.Controls[i];
            }
            for (var i = 0; i < root.Controls.Count; ++i)
            {
                Control result;
                for (var k = 0; k < root.Controls[i].Controls.Count; ++k)
                {
                    result = FindControl(root.Controls[i].Controls[k], target);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }
        /*******************************************************************************/

        // Need to put these in separate class.//
        private bool PlaceNum(int row, int col)
        {
            if (row == BOARDLENGTH - 1 && col == BOARDLENGTH)
                return true;

            // Resets col and increments row
            if (col == BOARDLENGTH) {
                row += 1;
                col = 0;
            }

            // Create vector that stores numbers that can be used.
            List<int> numPool = new List<int>(BOARDLENGTH);
            for (int i = 0; i < BOARDLENGTH; i++)
                numPool.Add(i + 1);

            // Try a number and call function again.
            while (numPool.Count > 0) {
                int n = GetNum(ref numPool);
                if (board[row, col] == 0 && IsValid(row, col, n)) {
                    board[row, col] = n;

                    if (PlaceNum(row, col + 1))
                        return true;
                    else
                        board[row, col] = 0;
                }
            }
            return false;
        }

        private bool IsValid(int row, int col, int n)
        {
            // Row and col check
            for (int i = 0; i < BOARDLENGTH; i++) {
                if (board[row, i] == n || board[i, col] == n)
                    return false;
            }

            // 3x3 square check
            int sqR = (row / 3) * 3;  // These vars get top left pos of the current 3x3 square.
            int sqC = (col / 3) * 3;  // int division truncates decimal places.

            for (int i = sqR; i < (sqR + 3); i++) {
                for (int j = sqC; j < (sqC + 3); j++) {
                    if (board[i, j] == n)
                        return false;
                }
            }
            return true;
        }

        private int GetNum(ref List<int> numPool)
        {
            //Random rnd = new Random();
            int posOfNum = rnd.Next(0, numPool.Count);     // Gets a random position for numPool. Doing this instead of generating
                                                        // an actual number because that would require a loop to check and remove from vector.
            int temp = numPool[posOfNum];
            numPool.Remove(temp);

            return temp;
        }

        /****************************************/
    }
}
