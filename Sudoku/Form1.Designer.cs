﻿namespace Sudoku
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.table3x3 = new System.Windows.Forms.TableLayoutPanel();
            this.square33 = new System.Windows.Forms.TableLayoutPanel();
            this.label80 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.square32 = new System.Windows.Forms.TableLayoutPanel();
            this.label77 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.square31 = new System.Windows.Forms.TableLayoutPanel();
            this.label74 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.square23 = new System.Windows.Forms.TableLayoutPanel();
            this.label34 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.square22 = new System.Windows.Forms.TableLayoutPanel();
            this.label50 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.square21 = new System.Windows.Forms.TableLayoutPanel();
            this.label47 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.square13 = new System.Windows.Forms.TableLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.square12 = new System.Windows.Forms.TableLayoutPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.square11 = new System.Windows.Forms.TableLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.buttonGenBoard = new System.Windows.Forms.Button();
            this.labelInfo = new System.Windows.Forms.Label();
            this.textBoxNumToHide = new System.Windows.Forms.TextBox();
            this.labelNumToHide = new System.Windows.Forms.Label();
            this.table3x3.SuspendLayout();
            this.square33.SuspendLayout();
            this.square32.SuspendLayout();
            this.square31.SuspendLayout();
            this.square23.SuspendLayout();
            this.square22.SuspendLayout();
            this.square21.SuspendLayout();
            this.square13.SuspendLayout();
            this.square12.SuspendLayout();
            this.square11.SuspendLayout();
            this.SuspendLayout();
            // 
            // table3x3
            // 
            this.table3x3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.table3x3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.table3x3.ColumnCount = 3;
            this.table3x3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.table3x3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.table3x3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.table3x3.Controls.Add(this.square33, 2, 2);
            this.table3x3.Controls.Add(this.square32, 1, 2);
            this.table3x3.Controls.Add(this.square31, 0, 2);
            this.table3x3.Controls.Add(this.square23, 2, 1);
            this.table3x3.Controls.Add(this.square22, 1, 1);
            this.table3x3.Controls.Add(this.square21, 0, 1);
            this.table3x3.Controls.Add(this.square13, 2, 0);
            this.table3x3.Controls.Add(this.square12, 1, 0);
            this.table3x3.Controls.Add(this.square11, 0, 0);
            this.table3x3.Location = new System.Drawing.Point(56, 58);
            this.table3x3.Margin = new System.Windows.Forms.Padding(2);
            this.table3x3.MaximumSize = new System.Drawing.Size(335, 335);
            this.table3x3.MinimumSize = new System.Drawing.Size(335, 335);
            this.table3x3.Name = "table3x3";
            this.table3x3.RowCount = 3;
            this.table3x3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.table3x3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.table3x3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.table3x3.Size = new System.Drawing.Size(335, 335);
            this.table3x3.TabIndex = 0;
            // 
            // square33
            // 
            this.square33.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square33.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square33.ColumnCount = 3;
            this.square33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square33.Controls.Add(this.label80, 0, 2);
            this.square33.Controls.Add(this.label61, 0, 0);
            this.square33.Controls.Add(this.label62, 1, 0);
            this.square33.Controls.Add(this.label63, 2, 0);
            this.square33.Controls.Add(this.label70, 0, 1);
            this.square33.Controls.Add(this.label71, 1, 1);
            this.square33.Controls.Add(this.label72, 2, 1);
            this.square33.Controls.Add(this.label81, 2, 2);
            this.square33.Controls.Add(this.label79, 0, 2);
            this.square33.Location = new System.Drawing.Point(227, 227);
            this.square33.Name = "square33";
            this.square33.RowCount = 3;
            this.square33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square33.Size = new System.Drawing.Size(103, 103);
            this.square33.TabIndex = 8;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.BackColor = System.Drawing.SystemColors.Control;
            this.label80.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label80.Location = new System.Drawing.Point(36, 70);
            this.label80.Margin = new System.Windows.Forms.Padding(1);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(31, 31);
            this.label80.TabIndex = 11;
            this.label80.Text = "label80";
            this.label80.Click += new System.EventHandler(this.label_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.SystemColors.Control;
            this.label61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label61.Location = new System.Drawing.Point(2, 2);
            this.label61.Margin = new System.Windows.Forms.Padding(1);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(31, 31);
            this.label61.TabIndex = 4;
            this.label61.Text = "label61";
            this.label61.Click += new System.EventHandler(this.label_Click);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.SystemColors.Control;
            this.label62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label62.Location = new System.Drawing.Point(36, 2);
            this.label62.Margin = new System.Windows.Forms.Padding(1);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(31, 31);
            this.label62.TabIndex = 5;
            this.label62.Text = "label62";
            this.label62.Click += new System.EventHandler(this.label_Click);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.SystemColors.Control;
            this.label63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label63.Location = new System.Drawing.Point(70, 2);
            this.label63.Margin = new System.Windows.Forms.Padding(1);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(31, 31);
            this.label63.TabIndex = 6;
            this.label63.Text = "label63";
            this.label63.Click += new System.EventHandler(this.label_Click);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.SystemColors.Control;
            this.label70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label70.Location = new System.Drawing.Point(2, 36);
            this.label70.Margin = new System.Windows.Forms.Padding(1);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(31, 31);
            this.label70.TabIndex = 7;
            this.label70.Text = "label70";
            this.label70.Click += new System.EventHandler(this.label_Click);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.BackColor = System.Drawing.SystemColors.Control;
            this.label71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label71.Location = new System.Drawing.Point(36, 36);
            this.label71.Margin = new System.Windows.Forms.Padding(1);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(31, 31);
            this.label71.TabIndex = 8;
            this.label71.Text = "label71";
            this.label71.Click += new System.EventHandler(this.label_Click);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.SystemColors.Control;
            this.label72.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label72.Location = new System.Drawing.Point(70, 36);
            this.label72.Margin = new System.Windows.Forms.Padding(1);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(31, 31);
            this.label72.TabIndex = 9;
            this.label72.Text = "label72";
            this.label72.Click += new System.EventHandler(this.label_Click);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.BackColor = System.Drawing.SystemColors.Control;
            this.label81.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label81.Location = new System.Drawing.Point(70, 70);
            this.label81.Margin = new System.Windows.Forms.Padding(1);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(31, 31);
            this.label81.TabIndex = 12;
            this.label81.Text = "label81";
            this.label81.Click += new System.EventHandler(this.label_Click);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.BackColor = System.Drawing.SystemColors.Control;
            this.label79.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label79.Location = new System.Drawing.Point(2, 70);
            this.label79.Margin = new System.Windows.Forms.Padding(1);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(31, 31);
            this.label79.TabIndex = 10;
            this.label79.Text = "label79";
            this.label79.Click += new System.EventHandler(this.label_Click);
            // 
            // square32
            // 
            this.square32.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square32.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square32.ColumnCount = 3;
            this.square32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square32.Controls.Add(this.label77, 0, 2);
            this.square32.Controls.Add(this.label58, 0, 0);
            this.square32.Controls.Add(this.label59, 1, 0);
            this.square32.Controls.Add(this.label60, 2, 0);
            this.square32.Controls.Add(this.label67, 0, 1);
            this.square32.Controls.Add(this.label68, 1, 1);
            this.square32.Controls.Add(this.label69, 2, 1);
            this.square32.Controls.Add(this.label78, 2, 2);
            this.square32.Controls.Add(this.label76, 0, 2);
            this.square32.Location = new System.Drawing.Point(116, 227);
            this.square32.Name = "square32";
            this.square32.RowCount = 3;
            this.square32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square32.Size = new System.Drawing.Size(103, 103);
            this.square32.TabIndex = 7;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label77.Location = new System.Drawing.Point(36, 70);
            this.label77.Margin = new System.Windows.Forms.Padding(1);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(31, 31);
            this.label77.TabIndex = 11;
            this.label77.Text = "label77";
            this.label77.Click += new System.EventHandler(this.label_Click);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label58.Location = new System.Drawing.Point(2, 2);
            this.label58.Margin = new System.Windows.Forms.Padding(1);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(31, 31);
            this.label58.TabIndex = 4;
            this.label58.Text = "label58";
            this.label58.Click += new System.EventHandler(this.label_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label59.Location = new System.Drawing.Point(36, 2);
            this.label59.Margin = new System.Windows.Forms.Padding(1);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(31, 31);
            this.label59.TabIndex = 5;
            this.label59.Text = "label59";
            this.label59.Click += new System.EventHandler(this.label_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label60.Location = new System.Drawing.Point(70, 2);
            this.label60.Margin = new System.Windows.Forms.Padding(1);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(31, 31);
            this.label60.TabIndex = 6;
            this.label60.Text = "label60";
            this.label60.Click += new System.EventHandler(this.label_Click);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label67.Location = new System.Drawing.Point(2, 36);
            this.label67.Margin = new System.Windows.Forms.Padding(1);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(31, 31);
            this.label67.TabIndex = 7;
            this.label67.Text = "label67";
            this.label67.Click += new System.EventHandler(this.label_Click);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label68.Location = new System.Drawing.Point(36, 36);
            this.label68.Margin = new System.Windows.Forms.Padding(1);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(31, 31);
            this.label68.TabIndex = 8;
            this.label68.Text = "label68";
            this.label68.Click += new System.EventHandler(this.label_Click);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label69.Location = new System.Drawing.Point(70, 36);
            this.label69.Margin = new System.Windows.Forms.Padding(1);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(31, 31);
            this.label69.TabIndex = 9;
            this.label69.Text = "label69";
            this.label69.Click += new System.EventHandler(this.label_Click);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label78.Location = new System.Drawing.Point(70, 70);
            this.label78.Margin = new System.Windows.Forms.Padding(1);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(31, 31);
            this.label78.TabIndex = 12;
            this.label78.Text = "label78";
            this.label78.Click += new System.EventHandler(this.label_Click);
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label76.Location = new System.Drawing.Point(2, 70);
            this.label76.Margin = new System.Windows.Forms.Padding(1);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(31, 31);
            this.label76.TabIndex = 10;
            this.label76.Text = "label76";
            this.label76.Click += new System.EventHandler(this.label_Click);
            // 
            // square31
            // 
            this.square31.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square31.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square31.ColumnCount = 3;
            this.square31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square31.Controls.Add(this.label74, 0, 2);
            this.square31.Controls.Add(this.label55, 0, 0);
            this.square31.Controls.Add(this.label56, 1, 0);
            this.square31.Controls.Add(this.label57, 2, 0);
            this.square31.Controls.Add(this.label64, 0, 1);
            this.square31.Controls.Add(this.label65, 1, 1);
            this.square31.Controls.Add(this.label66, 2, 1);
            this.square31.Controls.Add(this.label75, 2, 2);
            this.square31.Controls.Add(this.label73, 0, 2);
            this.square31.Location = new System.Drawing.Point(5, 227);
            this.square31.Name = "square31";
            this.square31.RowCount = 3;
            this.square31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square31.Size = new System.Drawing.Size(103, 103);
            this.square31.TabIndex = 6;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label74.Location = new System.Drawing.Point(36, 70);
            this.label74.Margin = new System.Windows.Forms.Padding(1);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(31, 31);
            this.label74.TabIndex = 11;
            this.label74.Text = "label74";
            this.label74.Click += new System.EventHandler(this.label_Click);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label55.Location = new System.Drawing.Point(2, 2);
            this.label55.Margin = new System.Windows.Forms.Padding(1);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(31, 31);
            this.label55.TabIndex = 4;
            this.label55.Text = "label55";
            this.label55.Click += new System.EventHandler(this.label_Click);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label56.Location = new System.Drawing.Point(36, 2);
            this.label56.Margin = new System.Windows.Forms.Padding(1);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(31, 31);
            this.label56.TabIndex = 5;
            this.label56.Text = "label56";
            this.label56.Click += new System.EventHandler(this.label_Click);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label57.Location = new System.Drawing.Point(70, 2);
            this.label57.Margin = new System.Windows.Forms.Padding(1);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(31, 31);
            this.label57.TabIndex = 6;
            this.label57.Text = "label57";
            this.label57.Click += new System.EventHandler(this.label_Click);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label64.Location = new System.Drawing.Point(2, 36);
            this.label64.Margin = new System.Windows.Forms.Padding(1);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(31, 31);
            this.label64.TabIndex = 7;
            this.label64.Text = "label64";
            this.label64.Click += new System.EventHandler(this.label_Click);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label65.Location = new System.Drawing.Point(36, 36);
            this.label65.Margin = new System.Windows.Forms.Padding(1);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(31, 31);
            this.label65.TabIndex = 8;
            this.label65.Text = "label65";
            this.label65.Click += new System.EventHandler(this.label_Click);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label66.Location = new System.Drawing.Point(70, 36);
            this.label66.Margin = new System.Windows.Forms.Padding(1);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(31, 31);
            this.label66.TabIndex = 9;
            this.label66.Text = "label66";
            this.label66.Click += new System.EventHandler(this.label_Click);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label75.Location = new System.Drawing.Point(70, 70);
            this.label75.Margin = new System.Windows.Forms.Padding(1);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(31, 31);
            this.label75.TabIndex = 12;
            this.label75.Text = "label75";
            this.label75.Click += new System.EventHandler(this.label_Click);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label73.Location = new System.Drawing.Point(2, 70);
            this.label73.Margin = new System.Windows.Forms.Padding(1);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(31, 31);
            this.label73.TabIndex = 10;
            this.label73.Text = "label73";
            this.label73.Click += new System.EventHandler(this.label_Click);
            // 
            // square23
            // 
            this.square23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square23.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square23.ColumnCount = 3;
            this.square23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square23.Controls.Add(this.label34, 0, 0);
            this.square23.Controls.Add(this.label43, 0, 1);
            this.square23.Controls.Add(this.label44, 1, 1);
            this.square23.Controls.Add(this.label45, 2, 1);
            this.square23.Controls.Add(this.label54, 2, 2);
            this.square23.Controls.Add(this.label35, 1, 0);
            this.square23.Controls.Add(this.label36, 2, 0);
            this.square23.Controls.Add(this.label53, 1, 2);
            this.square23.Controls.Add(this.label52, 0, 2);
            this.square23.Location = new System.Drawing.Point(227, 116);
            this.square23.Name = "square23";
            this.square23.RowCount = 3;
            this.square23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.square23.Size = new System.Drawing.Size(103, 103);
            this.square23.TabIndex = 5;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Location = new System.Drawing.Point(2, 2);
            this.label34.Margin = new System.Windows.Forms.Padding(1);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 31);
            this.label34.TabIndex = 0;
            this.label34.Text = "label34";
            this.label34.Click += new System.EventHandler(this.label_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label43.Location = new System.Drawing.Point(2, 36);
            this.label43.Margin = new System.Windows.Forms.Padding(1);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(31, 31);
            this.label43.TabIndex = 7;
            this.label43.Text = "label43";
            this.label43.Click += new System.EventHandler(this.label_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Location = new System.Drawing.Point(36, 36);
            this.label44.Margin = new System.Windows.Forms.Padding(1);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(31, 31);
            this.label44.TabIndex = 8;
            this.label44.Text = "label44";
            this.label44.Click += new System.EventHandler(this.label_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label45.Location = new System.Drawing.Point(70, 36);
            this.label45.Margin = new System.Windows.Forms.Padding(1);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(31, 31);
            this.label45.TabIndex = 9;
            this.label45.Text = "label45";
            this.label45.Click += new System.EventHandler(this.label_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label54.Location = new System.Drawing.Point(70, 70);
            this.label54.Margin = new System.Windows.Forms.Padding(1);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 31);
            this.label54.TabIndex = 12;
            this.label54.Text = "label54";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label54.Click += new System.EventHandler(this.label_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Location = new System.Drawing.Point(36, 2);
            this.label35.Margin = new System.Windows.Forms.Padding(1);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(31, 31);
            this.label35.TabIndex = 5;
            this.label35.Text = "label35";
            this.label35.Click += new System.EventHandler(this.label_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Location = new System.Drawing.Point(70, 2);
            this.label36.Margin = new System.Windows.Forms.Padding(1);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(31, 31);
            this.label36.TabIndex = 6;
            this.label36.Text = "label36";
            this.label36.Click += new System.EventHandler(this.label_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label53.Location = new System.Drawing.Point(36, 70);
            this.label53.Margin = new System.Windows.Forms.Padding(1);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(31, 31);
            this.label53.TabIndex = 11;
            this.label53.Text = "label53";
            this.label53.Click += new System.EventHandler(this.label_Click);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label52.Location = new System.Drawing.Point(2, 70);
            this.label52.Margin = new System.Windows.Forms.Padding(1);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(31, 31);
            this.label52.TabIndex = 10;
            this.label52.Text = "label52";
            this.label52.Click += new System.EventHandler(this.label_Click);
            // 
            // square22
            // 
            this.square22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square22.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square22.ColumnCount = 3;
            this.square22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square22.Controls.Add(this.label50, 0, 2);
            this.square22.Controls.Add(this.label31, 0, 0);
            this.square22.Controls.Add(this.label32, 1, 0);
            this.square22.Controls.Add(this.label33, 2, 0);
            this.square22.Controls.Add(this.label40, 0, 1);
            this.square22.Controls.Add(this.label41, 1, 1);
            this.square22.Controls.Add(this.label42, 2, 1);
            this.square22.Controls.Add(this.label51, 2, 2);
            this.square22.Controls.Add(this.label49, 0, 2);
            this.square22.Location = new System.Drawing.Point(116, 116);
            this.square22.Name = "square22";
            this.square22.RowCount = 3;
            this.square22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square22.Size = new System.Drawing.Size(103, 103);
            this.square22.TabIndex = 4;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label50.Location = new System.Drawing.Point(36, 70);
            this.label50.Margin = new System.Windows.Forms.Padding(1);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(31, 31);
            this.label50.TabIndex = 11;
            this.label50.Text = "label50";
            this.label50.Click += new System.EventHandler(this.label_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Location = new System.Drawing.Point(2, 2);
            this.label31.Margin = new System.Windows.Forms.Padding(1);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 31);
            this.label31.TabIndex = 4;
            this.label31.Text = "label31";
            this.label31.Click += new System.EventHandler(this.label_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Location = new System.Drawing.Point(36, 2);
            this.label32.Margin = new System.Windows.Forms.Padding(1);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(31, 31);
            this.label32.TabIndex = 5;
            this.label32.Text = "label32";
            this.label32.Click += new System.EventHandler(this.label_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Location = new System.Drawing.Point(70, 2);
            this.label33.Margin = new System.Windows.Forms.Padding(1);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(31, 31);
            this.label33.TabIndex = 6;
            this.label33.Text = "label33";
            this.label33.Click += new System.EventHandler(this.label_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label40.Location = new System.Drawing.Point(2, 36);
            this.label40.Margin = new System.Windows.Forms.Padding(1);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(31, 31);
            this.label40.TabIndex = 7;
            this.label40.Text = "label40";
            this.label40.Click += new System.EventHandler(this.label_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label41.Location = new System.Drawing.Point(36, 36);
            this.label41.Margin = new System.Windows.Forms.Padding(1);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(31, 31);
            this.label41.TabIndex = 8;
            this.label41.Text = "label41";
            this.label41.Click += new System.EventHandler(this.label_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label42.Location = new System.Drawing.Point(70, 36);
            this.label42.Margin = new System.Windows.Forms.Padding(1);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(31, 31);
            this.label42.TabIndex = 9;
            this.label42.Text = "label42";
            this.label42.Click += new System.EventHandler(this.label_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label51.Location = new System.Drawing.Point(70, 70);
            this.label51.Margin = new System.Windows.Forms.Padding(1);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(31, 31);
            this.label51.TabIndex = 12;
            this.label51.Text = "label51";
            this.label51.Click += new System.EventHandler(this.label_Click);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Location = new System.Drawing.Point(2, 70);
            this.label49.Margin = new System.Windows.Forms.Padding(1);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(31, 31);
            this.label49.TabIndex = 10;
            this.label49.Text = "label49";
            this.label49.Click += new System.EventHandler(this.label_Click);
            // 
            // square21
            // 
            this.square21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square21.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square21.ColumnCount = 3;
            this.square21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square21.Controls.Add(this.label47, 0, 2);
            this.square21.Controls.Add(this.label28, 0, 0);
            this.square21.Controls.Add(this.label29, 1, 0);
            this.square21.Controls.Add(this.label30, 2, 0);
            this.square21.Controls.Add(this.label37, 0, 1);
            this.square21.Controls.Add(this.label38, 1, 1);
            this.square21.Controls.Add(this.label39, 2, 1);
            this.square21.Controls.Add(this.label48, 2, 2);
            this.square21.Controls.Add(this.label46, 0, 2);
            this.square21.Location = new System.Drawing.Point(5, 116);
            this.square21.Name = "square21";
            this.square21.RowCount = 3;
            this.square21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square21.Size = new System.Drawing.Size(103, 103);
            this.square21.TabIndex = 3;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label47.Location = new System.Drawing.Point(36, 70);
            this.label47.Margin = new System.Windows.Forms.Padding(1);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(31, 31);
            this.label47.TabIndex = 11;
            this.label47.Text = "label47";
            this.label47.Click += new System.EventHandler(this.label_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Location = new System.Drawing.Point(2, 2);
            this.label28.Margin = new System.Windows.Forms.Padding(1);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(31, 31);
            this.label28.TabIndex = 4;
            this.label28.Text = "label28";
            this.label28.Click += new System.EventHandler(this.label_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Location = new System.Drawing.Point(36, 2);
            this.label29.Margin = new System.Windows.Forms.Padding(1);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(31, 31);
            this.label29.TabIndex = 5;
            this.label29.Text = "label29";
            this.label29.Click += new System.EventHandler(this.label_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Location = new System.Drawing.Point(70, 2);
            this.label30.Margin = new System.Windows.Forms.Padding(1);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(31, 31);
            this.label30.TabIndex = 6;
            this.label30.Text = "label30";
            this.label30.Click += new System.EventHandler(this.label_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Location = new System.Drawing.Point(2, 36);
            this.label37.Margin = new System.Windows.Forms.Padding(1);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(31, 31);
            this.label37.TabIndex = 7;
            this.label37.Text = "label37";
            this.label37.Click += new System.EventHandler(this.label_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Location = new System.Drawing.Point(36, 36);
            this.label38.Margin = new System.Windows.Forms.Padding(1);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(31, 31);
            this.label38.TabIndex = 8;
            this.label38.Text = "label38";
            this.label38.Click += new System.EventHandler(this.label_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.Location = new System.Drawing.Point(70, 36);
            this.label39.Margin = new System.Windows.Forms.Padding(1);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(31, 31);
            this.label39.TabIndex = 9;
            this.label39.Text = "label39";
            this.label39.Click += new System.EventHandler(this.label_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label48.Location = new System.Drawing.Point(70, 70);
            this.label48.Margin = new System.Windows.Forms.Padding(1);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(31, 31);
            this.label48.TabIndex = 12;
            this.label48.Text = "label48";
            this.label48.Click += new System.EventHandler(this.label_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label46.Location = new System.Drawing.Point(2, 70);
            this.label46.Margin = new System.Windows.Forms.Padding(1);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(31, 31);
            this.label46.TabIndex = 10;
            this.label46.Text = "label46";
            this.label46.Click += new System.EventHandler(this.label_Click);
            // 
            // square13
            // 
            this.square13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square13.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square13.ColumnCount = 3;
            this.square13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square13.Controls.Add(this.label27, 2, 2);
            this.square13.Controls.Add(this.label26, 1, 2);
            this.square13.Controls.Add(this.label25, 0, 2);
            this.square13.Controls.Add(this.label18, 2, 1);
            this.square13.Controls.Add(this.label17, 1, 1);
            this.square13.Controls.Add(this.label16, 0, 1);
            this.square13.Controls.Add(this.label7, 0, 0);
            this.square13.Controls.Add(this.label8, 1, 0);
            this.square13.Controls.Add(this.label9, 2, 0);
            this.square13.Location = new System.Drawing.Point(227, 5);
            this.square13.Name = "square13";
            this.square13.RowCount = 3;
            this.square13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square13.Size = new System.Drawing.Size(103, 103);
            this.square13.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Location = new System.Drawing.Point(70, 70);
            this.label27.Margin = new System.Windows.Forms.Padding(1);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(31, 31);
            this.label27.TabIndex = 15;
            this.label27.Text = "label27";
            this.label27.Click += new System.EventHandler(this.label_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Location = new System.Drawing.Point(36, 70);
            this.label26.Margin = new System.Windows.Forms.Padding(1);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(31, 31);
            this.label26.TabIndex = 14;
            this.label26.Text = "label26";
            this.label26.Click += new System.EventHandler(this.label_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Location = new System.Drawing.Point(2, 70);
            this.label25.Margin = new System.Windows.Forms.Padding(1);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 31);
            this.label25.TabIndex = 13;
            this.label25.Text = "label25";
            this.label25.Click += new System.EventHandler(this.label_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(70, 36);
            this.label18.Margin = new System.Windows.Forms.Padding(1);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 31);
            this.label18.TabIndex = 12;
            this.label18.Text = "label18";
            this.label18.Click += new System.EventHandler(this.label_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Location = new System.Drawing.Point(36, 36);
            this.label17.Margin = new System.Windows.Forms.Padding(1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 31);
            this.label17.TabIndex = 11;
            this.label17.Text = "label17";
            this.label17.Click += new System.EventHandler(this.label_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(2, 36);
            this.label16.Margin = new System.Windows.Forms.Padding(1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 31);
            this.label16.TabIndex = 10;
            this.label16.Text = "label16";
            this.label16.Click += new System.EventHandler(this.label_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(2, 2);
            this.label7.Margin = new System.Windows.Forms.Padding(1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 31);
            this.label7.TabIndex = 0;
            this.label7.Text = "label7";
            this.label7.Click += new System.EventHandler(this.label_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(36, 2);
            this.label8.Margin = new System.Windows.Forms.Padding(1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 31);
            this.label8.TabIndex = 1;
            this.label8.Text = "label8";
            this.label8.Click += new System.EventHandler(this.label_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(70, 2);
            this.label9.Margin = new System.Windows.Forms.Padding(1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 31);
            this.label9.TabIndex = 2;
            this.label9.Text = "label9";
            this.label9.Click += new System.EventHandler(this.label_Click);
            // 
            // square12
            // 
            this.square12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square12.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square12.ColumnCount = 3;
            this.square12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square12.Controls.Add(this.label24, 2, 2);
            this.square12.Controls.Add(this.label23, 1, 2);
            this.square12.Controls.Add(this.label22, 0, 2);
            this.square12.Controls.Add(this.label15, 2, 1);
            this.square12.Controls.Add(this.label14, 1, 1);
            this.square12.Controls.Add(this.label13, 0, 1);
            this.square12.Controls.Add(this.label6, 2, 0);
            this.square12.Controls.Add(this.label5, 1, 0);
            this.square12.Controls.Add(this.label4, 0, 0);
            this.square12.Location = new System.Drawing.Point(116, 5);
            this.square12.Name = "square12";
            this.square12.RowCount = 3;
            this.square12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square12.Size = new System.Drawing.Size(103, 103);
            this.square12.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Location = new System.Drawing.Point(70, 70);
            this.label24.Margin = new System.Windows.Forms.Padding(1);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 31);
            this.label24.TabIndex = 12;
            this.label24.Text = "label24";
            this.label24.Click += new System.EventHandler(this.label_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Location = new System.Drawing.Point(36, 70);
            this.label23.Margin = new System.Windows.Forms.Padding(1);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(31, 31);
            this.label23.TabIndex = 11;
            this.label23.Text = "label23";
            this.label23.Click += new System.EventHandler(this.label_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Location = new System.Drawing.Point(2, 70);
            this.label22.Margin = new System.Windows.Forms.Padding(1);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(31, 31);
            this.label22.TabIndex = 10;
            this.label22.Text = "label22";
            this.label22.Click += new System.EventHandler(this.label_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(70, 36);
            this.label15.Margin = new System.Windows.Forms.Padding(1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(31, 31);
            this.label15.TabIndex = 9;
            this.label15.Text = "label15";
            this.label15.Click += new System.EventHandler(this.label_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(36, 36);
            this.label14.Margin = new System.Windows.Forms.Padding(1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 31);
            this.label14.TabIndex = 8;
            this.label14.Text = "label14";
            this.label14.Click += new System.EventHandler(this.label_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(2, 36);
            this.label13.Margin = new System.Windows.Forms.Padding(1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 31);
            this.label13.TabIndex = 7;
            this.label13.Text = "label13";
            this.label13.Click += new System.EventHandler(this.label_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(70, 2);
            this.label6.Margin = new System.Windows.Forms.Padding(1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 31);
            this.label6.TabIndex = 2;
            this.label6.Text = "label6";
            this.label6.Click += new System.EventHandler(this.label_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(36, 2);
            this.label5.Margin = new System.Windows.Forms.Padding(1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 31);
            this.label5.TabIndex = 1;
            this.label5.Text = "label5";
            this.label5.Click += new System.EventHandler(this.label_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(2, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "label4";
            this.label4.Click += new System.EventHandler(this.label_Click);
            // 
            // square11
            // 
            this.square11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.square11.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.square11.ColumnCount = 3;
            this.square11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square11.Controls.Add(this.label20, 0, 2);
            this.square11.Controls.Add(this.label1, 0, 0);
            this.square11.Controls.Add(this.label2, 1, 0);
            this.square11.Controls.Add(this.label3, 2, 0);
            this.square11.Controls.Add(this.label10, 0, 1);
            this.square11.Controls.Add(this.label12, 2, 1);
            this.square11.Controls.Add(this.label11, 1, 1);
            this.square11.Controls.Add(this.label21, 1, 2);
            this.square11.Controls.Add(this.label19, 0, 2);
            this.square11.Location = new System.Drawing.Point(5, 5);
            this.square11.Name = "square11";
            this.square11.RowCount = 3;
            this.square11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.square11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.square11.Size = new System.Drawing.Size(103, 103);
            this.square11.TabIndex = 0;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Location = new System.Drawing.Point(36, 70);
            this.label20.Margin = new System.Windows.Forms.Padding(1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(31, 31);
            this.label20.TabIndex = 8;
            this.label20.Text = "label20";
            this.label20.Click += new System.EventHandler(this.label_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(2, 2);
            this.label1.Margin = new System.Windows.Forms.Padding(1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            this.label1.Click += new System.EventHandler(this.label_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(36, 2);
            this.label2.Margin = new System.Windows.Forms.Padding(1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "label2";
            this.label2.Click += new System.EventHandler(this.label_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(70, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(1);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "label3";
            this.label3.Click += new System.EventHandler(this.label_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(2, 36);
            this.label10.Margin = new System.Windows.Forms.Padding(1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 31);
            this.label10.TabIndex = 3;
            this.label10.Text = "label10";
            this.label10.Click += new System.EventHandler(this.label_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(70, 36);
            this.label12.Margin = new System.Windows.Forms.Padding(1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(31, 31);
            this.label12.TabIndex = 6;
            this.label12.Text = "label12";
            this.label12.Click += new System.EventHandler(this.label_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(36, 36);
            this.label11.Margin = new System.Windows.Forms.Padding(1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 31);
            this.label11.TabIndex = 5;
            this.label11.Text = "label11";
            this.label11.Click += new System.EventHandler(this.label_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Location = new System.Drawing.Point(70, 70);
            this.label21.Margin = new System.Windows.Forms.Padding(1);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(31, 31);
            this.label21.TabIndex = 9;
            this.label21.Text = "label21";
            this.label21.Click += new System.EventHandler(this.label_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Location = new System.Drawing.Point(2, 70);
            this.label19.Margin = new System.Windows.Forms.Padding(1);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(31, 31);
            this.label19.TabIndex = 7;
            this.label19.Text = "label19";
            this.label19.Click += new System.EventHandler(this.label_Click);
            // 
            // buttonGenBoard
            // 
            this.buttonGenBoard.Font = new System.Drawing.Font("Moire", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGenBoard.Location = new System.Drawing.Point(419, 61);
            this.buttonGenBoard.Name = "buttonGenBoard";
            this.buttonGenBoard.Size = new System.Drawing.Size(107, 23);
            this.buttonGenBoard.TabIndex = 1;
            this.buttonGenBoard.Text = "Generate Board";
            this.buttonGenBoard.UseVisualStyleBackColor = true;
            this.buttonGenBoard.Click += new System.EventHandler(this.buttonGenBoard_Click);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.Font = new System.Drawing.Font("Moire", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.Location = new System.Drawing.Point(413, 176);
            this.labelInfo.MaximumSize = new System.Drawing.Size(150, 0);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(53, 14);
            this.labelInfo.TabIndex = 3;
            this.labelInfo.Text = "labelInfo";
            // 
            // textBoxNumToHide
            // 
            this.textBoxNumToHide.Location = new System.Drawing.Point(499, 96);
            this.textBoxNumToHide.MaxLength = 2;
            this.textBoxNumToHide.Name = "textBoxNumToHide";
            this.textBoxNumToHide.Size = new System.Drawing.Size(27, 20);
            this.textBoxNumToHide.TabIndex = 4;
            this.textBoxNumToHide.Text = "5";
            this.textBoxNumToHide.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelNumToHide
            // 
            this.labelNumToHide.AutoSize = true;
            this.labelNumToHide.Font = new System.Drawing.Font("Moire", 9F);
            this.labelNumToHide.Location = new System.Drawing.Point(416, 99);
            this.labelNumToHide.Name = "labelNumToHide";
            this.labelNumToHide.Size = new System.Drawing.Size(77, 15);
            this.labelNumToHide.TabIndex = 5;
            this.labelNumToHide.Text = "Hidden tiles:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 443);
            this.Controls.Add(this.textBoxNumToHide);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.buttonGenBoard);
            this.Controls.Add(this.table3x3);
            this.Controls.Add(this.labelNumToHide);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.table3x3.ResumeLayout(false);
            this.square33.ResumeLayout(false);
            this.square33.PerformLayout();
            this.square32.ResumeLayout(false);
            this.square32.PerformLayout();
            this.square31.ResumeLayout(false);
            this.square31.PerformLayout();
            this.square23.ResumeLayout(false);
            this.square23.PerformLayout();
            this.square22.ResumeLayout(false);
            this.square22.PerformLayout();
            this.square21.ResumeLayout(false);
            this.square21.PerformLayout();
            this.square13.ResumeLayout(false);
            this.square13.PerformLayout();
            this.square12.ResumeLayout(false);
            this.square12.PerformLayout();
            this.square11.ResumeLayout(false);
            this.square11.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel table3x3;
        private System.Windows.Forms.TableLayoutPanel square11;
        private System.Windows.Forms.TableLayoutPanel square33;
        private System.Windows.Forms.TableLayoutPanel square32;
        private System.Windows.Forms.TableLayoutPanel square31;
        private System.Windows.Forms.TableLayoutPanel square23;
        private System.Windows.Forms.TableLayoutPanel square22;
        private System.Windows.Forms.TableLayoutPanel square21;
        private System.Windows.Forms.TableLayoutPanel square13;
        private System.Windows.Forms.TableLayoutPanel square12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Button buttonGenBoard;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.TextBox textBoxNumToHide;
        private System.Windows.Forms.Label labelNumToHide;
    }
}

