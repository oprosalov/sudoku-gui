# Sudoku GUI

This is my first interesting project in C#, a very simple Sudoku game. I wrote it soon after writing a recursive Sudoku solver in C++ for a school assignment. 

Currently there are no difficulty settings, it only allows you to choose between 5-25 hidden tiles. 
I plan to add said difficulty settings (which require verification that a board is solvable), as well as port over my Dancing Links solver.